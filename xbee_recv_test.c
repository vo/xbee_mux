#include "xbee_serial.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int fd;

void signal_handler(int signum)
{
	printf("Caught signal %d\n", signum);
	close(fd);
	exit(signum);
}

int main(int argc, char * argv[])
{

	signal(SIGINT, signal_handler);

	char port[] = "/dev/ttyUSB1";

	fd = xbee_open("/dev/ttyUSB1", 115200);
	if (fd < 0)
		exit(fd);

	// create an fd set
	fd_set set;
	FD_ZERO(&set);
	FD_SET(fd, &set);

	// create an xbee_pkt structure
	xbee_pkt pkt;
	xbee_pkt_init(&pkt);

	int pkt_num = 0;

	while (1)
	{

		// wait for a byte to come in
		int r = select(FD_SETSIZE, &set, NULL, NULL, NULL);

		// if there was an error, exit with that error code
		if (r <= 0)
		{
			close(fd);
			exit(r);
		}

		// 1k buffer (nice big buffer)
		uint8_t buf[1024];

		// attempt to read up to 1KB from xbee
		r = read(fd, buf, 1024);
		int i, pktlen;
		for (i = 0; i < r; ++i)
		{
			int pktlen = xbee_parse(buf[i], &pkt);
			if (pktlen > 0 && pkt.data[3] == 0x90)
			{
				uint16_t framenum = (pkt.data[pkt.len - 3] << 8)
						| pkt.data[pkt.len - 2];
				printf("received %d, payload: %d\n", pkt_num++, framenum);
			}
		}

	}

	close(fd);

	return 0;
}
