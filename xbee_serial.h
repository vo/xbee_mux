/**
 * low level serial/xbee functions - Header
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef XBEE_SERIAL_H_
#define XBEE_SERIAL_H_

#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <ctype.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MIN(X,Y) ((X) < (Y) ? : (X) : (Y))

#define XBEE_ADDR_BROADCAST 0xFFFF
#define XBEE_ADDR_COORDINATOR 0x0000
#define XBEE_NETID_BROADCAST 0xFFFE
#define XBEE_NETID_ALL 0xFFFF
#define XBEE_NETID_ROUTERS 0xFFFC
#define XBEE_NETID_NON_SLEEPY 0xFFFD
#define XBEE_FRAME_ATCMD 0x08
#define XBEE_FRAME_TX64 0x00
#define XBEE_FRAME_RECV 0x90
#define XBEE_FRAME_TX_REQ 0x10

typedef enum {
	XBEE_PARSE_IDLE,
	XBEE_PARSE_START,
	XBEE_PARSE_LEN_HIGH,
	XBEE_PARSE_LEN_LOW,
	XBEE_PARSE_DATA_RCVD,
	XBEE_PARSE_COMPLETE
} xbee_parse_state;

typedef struct {
	xbee_parse_state state;	// which state the packet is in
	uint16_t expected_len;	// how long we expect the packet to be
	int escape_state;		// whether we're escaping the next byte
	int len;				// how long the packet is
	uint8_t data[1024];		// the packet raw data
} xbee_pkt;

int xbee_open(char* port, int baud);
int load_asciihex(uint8_t * buf, const char * hex);
int xbee_at_command(int fd, uint8_t * data, size_t n);
int xbee_write64(int fd, uint8_t * buf, unsigned int n, uint64_t dest);
int xbee_write_digimesh(int fd, uint8_t * buf, unsigned int n, uint64_t dest, uint16_t netid);
int xbee_escape(uint8_t * buf, unsigned int n);
int xbee_unescape(uint8_t * buf, unsigned int n);
int xbee_parse(uint8_t c, xbee_pkt * pkt);
void xbee_pkt_init(xbee_pkt * pkt);
uint8_t xbee_checksum(uint8_t * buf, unsigned int n);

#endif
