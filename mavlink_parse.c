/**
 * simple mavlink parser
 * does not interpret data, just checks for complete packets
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mavlink_parse.h"

/*
 * mavlink_parse
 * parse one character c of raw mavlink stream
 * if a packet is complete, the total length of mavlink packet is returned
 * otherwise, returns 0.
 */
int mavlink_parse(uint8_t c, mavlink_pkt * pkt) {
	assert(pkt);

	// special case: start delimiter received
	// but not inside a mavlink payload
	if(pkt->state != MAVLINK_PARSE_PAYLOAD && c == 0xFE) {
		// always consider 0xFE the start of packet
		pkt->state = MAVLINK_PARSE_LEN;
		pkt->len = 1;
		pkt->expected_len = 0;
		pkt->data[0] = c;
		return 0;
	} 

	// standard cases ----------------------------------------------
	switch(pkt->state) {
		case MAVLINK_PARSE_LEN: 
			// we are expecting a byte representing
			// the length of the payload
			pkt->state = MAVLINK_PARSE_PAYLOAD;
			pkt->len = 2;
			pkt->expected_len = 6+c;
			pkt->data[1] = c;
			break;
		case MAVLINK_PARSE_PAYLOAD:
			// we are expecting payload bytes.
			// keep reading bytes until the length
			// is equal to the expected length 
			pkt->data[pkt->len++] = c;
			if(pkt->len >= pkt->expected_len) {
				pkt->expected_len += 2; 
				pkt->state = MAVLINK_PARSE_CRC;
			}
			break;
		case MAVLINK_PARSE_CRC:
			// we are expecting the CRC byte.
			pkt->data[pkt->len++] = c;
			if(pkt->len >= pkt-> expected_len)
				pkt->state = MAVLINK_PARSE_COMPLETE;
			break;
		case MAVLINK_PARSE_COMPLETE: 
		default:
			// byte was received after packet complete
			// or in a state that isn't one of the above
			// just ignore the byte, it is meaningless
			break;
	}

	// if the above 
	if(pkt->state == MAVLINK_PARSE_COMPLETE) {
		return pkt->len;
	}

	return 0;

}

/*-----------------------------------------------------------------------------
 * mavlink_pkt_init
 * initialize an mavlink_pkt struct.
 */
void mavlink_pkt_init(mavlink_pkt * pkt) {
	assert(pkt);
	pkt->state = MAVLINK_PARSE_IDLE;
	pkt->expected_len = 0;
	pkt->len = 0;	
}