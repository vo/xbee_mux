/**
 * low level serial/xbee functions
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "xbee_serial.h"

/*
 * xbee_open
 * opens the serial port with given baud rate
 * returns the file descriptor of the port, or < 0 if failure.
 */
int xbee_open(char* port, int baud)
{
	// Open the serial port.
	// O_RDWR   : open for read/write
	// O_NOCTTY : do not cause terminal device to be controlling terminal
	int fd = open(port, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0)
	{
		fprintf(stderr, "xbee_open: Cannot open %s - %s\n", port, strerror(
		errno));
		return fd;
	}

	// Configure file reading.
	// F_SETFL : set file status flags.
	fcntl(fd, F_SETFL, 0);

	// configure termios options
	struct termios options;
	bzero(&options, sizeof(options));

	// 8N1 RAW
	options.c_cflag = CRTSCTS | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR | IGNBRK;
	options.c_oflag = 0;
	options.c_lflag = 0;

	// read timeout = 0
	// 1 char enough to return from read
	memset(options.c_cc, 0, sizeof(options.c_cc));
	options.c_cc[VTIME] = 0;
	options.c_cc[VMIN] = 1;

	// set baud option bits
	speed_t termbaud;
	switch (baud)
	{
	case 9600:
		termbaud = B9600;
		break;
	case 19200:
		termbaud = B19200;
		break;
	case 38400:
		termbaud = B38400;
		break;
	case 57600:
		termbaud = B57600;
		break;
	default:
		termbaud = B115200;
		break;
	}
	int r = cfsetispeed(&options, termbaud);
	if (r < 0)
	{
		fprintf(stderr, "xbee_configure: cfsetispeed() failed - %s\n",
				strerror(errno));
		return r;
	}
	r = cfsetospeed(&options, termbaud);
	if (r < 0)
	{
		fprintf(stderr, "xbee_configure: cfsetospeed() failed - %s\n",
				strerror(errno));
		return r;
	}

	// flush serial port
	r = tcflush(fd, TCIOFLUSH);
	if (r < 0)
	{
		fprintf(stderr, "xbee_configure: first tcflush() failed - %s\n",
				strerror(errno));
		return r;
	}
	// set attributes for serial port
	r = tcsetattr(fd, TCSANOW, &options);
	if (r < 0)
	{
		fprintf(stderr, "xbee_configure: tcsetattr() failed - %s\n",
				strerror(errno));
		return r;
	}
	// flush serial port again
	r = tcflush(fd, TCIOFLUSH);
	if (r < 0)
	{
		fprintf(stderr, "xbee_configure: second tcflush() failed - %s\n",
				strerror(errno));
		return r;
	}

	// return the file descriptor
	return fd;
}

/*-----------------------------------------------------------------------------
 * Load the given data (from ASCII hex string) into swap
 * Returns number of bytes read into swap
 */
int load_asciihex(uint8_t * buf, const char * hex)
{
	assert(buf);
	int i, j;
	int len = strlen(hex);

	// copy just the hex characters into a temporary buffer
	char hexstr[len];
	for (i = 0, j = 0; i < len; i++)
		if (isxdigit(hex[i]))
			hexstr[j++] = hex[i];
	hexstr[j] = '\0';
	len = j;
	int bytelen = len / 2;
	char tmp[2];

	// convert hex chars to bytes
	for (i = 0, j = 0; i < len; i += 2, j++)
	{
		tmp[0] = hexstr[i];
		tmp[1] = hexstr[i + 1];
		buf[j] = strtol(tmp, NULL, 16);
	}

	return bytelen;
}

/*-----------------------------------------------------------------------------
 * xbee_at_command (send an AT command)
 * given the AT command data
 * construct xbee packets and send immediately
 * return number of bytes sent, or < 0 if error.
 */
int xbee_at_command(int fd, uint8_t * data, size_t n)
{
	assert(data);

	if (n < 2)
	{
		fprintf(stderr,
				"Error: xbee_at_command expects at least 2 characters.");
		return -1;
	}

	uint8_t out[256]; // output buffer

	out[0] = 0x7E; 					// 1b: start delimiter
	out[1] = ((n + 2) >> 8) & 0xff;	// 2b: length
	out[2] = (n + 2) & 0xff;		//
	out[3] = XBEE_FRAME_ATCMD;		// 1b: AT command request
	out[4] = 0x01;					// 1b: frame id

	// copy n bytes from data -> buffer
	int p, q;
	for (p = 0, q = 5; p < n; ++p, ++q)
		out[q] = data[p];

	// checksum
	out[q] = xbee_checksum(out, q);

	// escape if needed
	q = xbee_escape(out + 1, q) + 1;

	int r = write(fd, out, q);
	return r;
}

/*-----------------------------------------------------------------------------
 * xbee_write64 (generic xbee tx64 request)
 * given buf with n bytes of data
 * construct xbee packets and send immediately to given address
 */
int xbee_write64(int fd, uint8_t * buf, unsigned int n, uint64_t dest)
{
	assert(buf);
	int result;
	int bytes_sent = 0;
	uint8_t out[256];	 // output buffer

	while (bytes_sent < n)
	{

		int rem = n - bytes_sent;
		int len = (rem < 100) ? rem : 100;

		out[0] = 0x7E;	 					// 1b: start delimiter
		out[1] = ((len + 11) >> 8) & 0xff; 	// 2b: length
		out[2] = (len + 11) & 0xff;		 	//
		out[3] = XBEE_FRAME_TX64;			// 1b: TX64 request
		out[4] = 0x00; 	 					// 1b: frame id

		out[5] = (dest >> 56) & 0xff; 		// 8b: destination address
		out[6] = (dest >> 48) & 0xff;
		out[7] = (dest >> 40) & 0xff;
		out[8] = (dest >> 32) & 0xff;
		out[9] = (dest >> 24) & 0xff;
		out[10] = (dest >> 16) & 0xff;
		out[11] = (dest >> 8) & 0xff;
		out[12] = dest & 0xff;

		out[13] = 0x00;						// 1b: options

		// payload -> buffer
		int p = bytes_sent, q = 14;
		while (p < (bytes_sent + len))
			out[q++] = buf[p++];

		// compute checksum (0xFF - sum & 0xFF) for all bytes
		out[q] = xbee_checksum(out, q);

		// escape all of the above (except for start delimiter)
		q = xbee_escape(out + 1, q) + 1;

		// send bytes
		result = write(fd, out, q);
		if (result < 0)
			return result;

		// advance bytes_sent
		bytes_sent += len;
	}

	return result;
}

/*-----------------------------------------------------------------------------
 * xbee_write64 (generic xbee write request)
 * given buf with n bytes of data
 * construct xbee packets and send immediately to given address
 */
int xbee_write_digimesh(int fd, uint8_t * buf, unsigned int n, uint64_t dest,
		uint16_t netid)
{
	assert(buf);
	int result;
	int bytes_sent = 0;
	uint8_t out[256];	 // output buffer

	while (bytes_sent < n)
	{

		int rem = n - bytes_sent;
		int len = (rem < 100) ? rem : 100;

		out[0] = 0x7E;	 					// 1b: start delimiter
		out[1] = ((len + 14) >> 8) & 0xff; 	// 2b: length
		out[2] = (len + 14) & 0xff;		 	//
		out[3] = XBEE_FRAME_TX_REQ;	 		// 1b: TX DM request
		out[4] = 0x00; 	 					// 1b: frame id

		out[5] = (dest >> 56) & 0xff; 		// 8b: destination address
		out[6] = (dest >> 48) & 0xff;
		out[7] = (dest >> 40) & 0xff;
		out[8] = (dest >> 32) & 0xff;
		out[9] = (dest >> 24) & 0xff;
		out[10] = (dest >> 16) & 0xff;
		out[11] = (dest >> 8) & 0xff;
		out[12] = dest & 0xff;

		out[13] = (netid >> 8) & 0xff;		// 2b: net id
		out[14] = netid & 0xff;

		out[15] = 0x00;						// 1b: radius
		out[16] = 0x00;						// 1b: options

		// payload -> buffer
		int p = bytes_sent, q = 17;
		while (p < (bytes_sent + len))
			out[q++] = buf[p++];

		// compute checksum (0xFF - sum & 0xFF) for all bytes
		out[q] = xbee_checksum(out, q);

		// escape all of the above (except for start delimiter)
		q = xbee_escape(out + 1, q) + 1;

		// send bytes
		result = write(fd, out, q);
		if (result < 0)
			return result;

		// advance bytes_sent
		bytes_sent += len;
	}

	return result;
}

/*-----------------------------------------------------------------------------
 * xbee_escape
 * escape the characters in buf.
 * return the new length of buf.
 * assumes that buf can expand to the new size.
 */
int xbee_escape(uint8_t * buf, unsigned int n)
{
	assert(buf);
	uint8_t outbuf[n * 2];
	int i = 0, j = 0;
	while (i < n)
	{
		uint8_t c = buf[i];
		switch (c)
		{
		case 0x7E:
		case 0x7D:
		case 0x11:
		case 0x13:
			outbuf[j++] = 0x7D;
			outbuf[j++] = buf[i++] ^ 0x20;
			break;
		default:
			outbuf[j++] = buf[i++];
			break;
		}
	}
	for (i = 0; i < j; ++i)
		buf[i] = outbuf[i];
	return j;
}

/*-----------------------------------------------------------------------------
 * xbee_unescape
 * unescape the characters in buf.
 * return the new length of buf.
 */
int xbee_unescape(uint8_t * buf, unsigned int n)
{
	assert(buf);

	uint8_t outbuf[n];

	int p, q;
	for (p = 0, q = 0; p < n; ++p)
	{
		if (buf[p] == 0x7D)
			outbuf[q++] = buf[++p] ^ 0x20;
		else
			outbuf[q++] = buf[p];
	}

	for(p=0; p < q; ++p)
		buf[p] = outbuf[p];

	return q;
}

/*-----------------------------------------------------------------------------
 * xbee_parse
 * parse one character c of xbee data
 * if a packet is complete, the total length is returned.
 * otherwise, returns 0.  
 */

int xbee_parse(uint8_t c, xbee_pkt * pkt)
{
	assert(pkt);
	int sum_b;

	// special cases ---------------------------------
	// start delimiter received
	if (c == 0x7E)
	{
		// always consider this the beginning of a new frame.
		pkt->state = XBEE_PARSE_START;
		pkt->len = 1;
		pkt->escape_state = 0;
		pkt->expected_len = 0;
		pkt->data[0] = c;
		return 0;
	}
	// or if escape character received
	else if (c == 0x7D)
	{
		// set the escape flag to 1
		pkt->escape_state = 1;
		return 0;
	}
	// or if we were already in escape mode
	else if (pkt->escape_state)
	{
		// un-escape the byte and move on!
		c ^= 0x20;
		pkt->escape_state = 0;
	}

	// standard cases --------------------------------
	switch (pkt->state)
	{
	// received start delim, so c should be length MSB
	case XBEE_PARSE_START:
		pkt->state = XBEE_PARSE_LEN_HIGH;
		pkt->data[1] = c;
		pkt->len = 2;
		break;
		// received MSB, so c should be length LSB
	case XBEE_PARSE_LEN_HIGH:
		pkt->state = XBEE_PARSE_LEN_LOW;
		pkt->data[2] = c;
		pkt->len = 3;
		pkt->expected_len = 3 + ((pkt->data[1] << 8) | (pkt->data[2]));
		break;
		// received LSB, so c is a frame data byte
	case XBEE_PARSE_LEN_LOW:
		pkt->data[pkt->len++] = c;
		if (pkt->len >= pkt->expected_len)
			pkt->state = XBEE_PARSE_DATA_RCVD;
		break;
		// finished getting frame data, so c is the checksum
	case XBEE_PARSE_DATA_RCVD:
		sum_b = xbee_checksum(pkt->data, pkt->len);
		if (sum_b == c)
		{ // checksum match!
			pkt->data[pkt->len++] = c;
			pkt->state = XBEE_PARSE_COMPLETE;
		}
		else
		{
			// checksum mismatch
			// in this case, drop the packet
			xbee_pkt_init(pkt);
		}
		break;
	default: // reset to idle
		xbee_pkt_init(pkt);
		break;
	}

	// if the packet was completed this round,
	// return pkt->len to indicate a full packet was received.
	if (pkt->state == XBEE_PARSE_COMPLETE)
		return pkt->len;

	return 0;
}

/*-----------------------------------------------------------------------------
 * xbee_checksum
 * given an xbee frame in buf with length n (not including checksum byte)
 * compute and return the checksum.
 */
uint8_t xbee_checksum(uint8_t * buf, unsigned int n)
{
	assert(buf);
	uint64_t sum = 0;
	int i;
	for (i = 3; i < n; ++i)
		sum += buf[i];
	return 0xFF - (uint8_t) (sum);
}

/*-----------------------------------------------------------------------------
 * xbee_pkt_init
 * initialize an xbee_pkt struct.
 */
void xbee_pkt_init(xbee_pkt * pkt)
{
	assert(pkt);
	pkt->state = XBEE_PARSE_IDLE;
	pkt->expected_len = 0;
	pkt->escape_state = 0;
	pkt->len = 0;
}

