/**
 * Opens a bidirectional bridge between an XBEE API V2 device
 * and either a new Pseudo TTY device, or a given serial device.
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <getopt.h>
#include <sys/time.h>
#include <time.h>

#include <pty.h>
#include <pthread.h>

#include "xbee_serial.h"
#include "mavlink_parse.h"

// global variables
pthread_t _pty_loop_thread;		// pty loop thread
pthread_t _xbee_loop_thread;	// xbee loop thread
char _addr_list_filename[128];	// the filename for the address list
uint64_t _addr_list[8];			// up to 8 addresses to include
size_t _num_addr;				// number of addresses in the list
int _pty_fd_list[8];			// list of the file descriptors for the pty
char _pty_name_list[8][128];	// names of the ptys
int _xbee_fd;					// the xbee file descriptor
char _xbee_port[128];			// the xbee port name
int _xbee_baud;					// the xbee baud rate

// signal handler
void sigint_handler(int signum)
{
	printf("Caught Interrupt, Exiting.\n");
	pthread_cancel(_pty_loop_thread);
	pthread_cancel(_xbee_loop_thread);
}

// manually print the 64-bit addresses
// this is needed on systems where the printf format pattern %016lX won't work
void print_address(uint64_t addr, char * pty) {
	int i;
	for(i=56; i>=0; i-=8) 
		printf("%02X", (uint8_t)((addr >> i) & 0xFF));
	printf(" -> %s\n", pty);
}

// s must be a 16 character hex string
// this function needed because strtol doesn't work on some systems
uint64_t strtouint64_t(char * s) {
	int i;
	uint64_t x = 0;
	char tmp[3];
	tmp[2] = '\0';
	for(i=0; i<16; i+=2) {
		tmp[0] = s[i];
		tmp[1] = s[i+1];
		uint64_t b = strtol(tmp, NULL, 16);
		b <<= (56 - (i*4));
		x |= b;		
	}
	return x;
} 


// initialize the address list from the address list file.
void init_addr_list()
{
	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	fp = fopen(_addr_list_filename, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "Could not open address list file.");
		exit(EXIT_FAILURE);
	}
	_num_addr = 0;
	while ((read = getline(&line, &len, fp)) != -1 && _num_addr < 8)
	{
		char addr_hex[17];
		int i, j;
		// strip non-hex digits
		for (i = 0, j = 0; i < read && j < 17; ++i)
		{
			if (isxdigit(line[i]))
			{
				addr_hex[j++] = line[i];
			}
		}
		addr_hex[16] = '\0';
		
		// convert to uint64_t
		uint64_t addr = strtouint64_t(addr_hex);
		_addr_list[_num_addr++] = addr;
	}
	free(line);
}

// read the command line parameters.
void init_cmdline(int argc, char ** argv)
{
	int c;
	strcpy(_addr_list_filename, "addr.list");
	strcpy(_xbee_port, "/dev/ttyUSB0");
	_xbee_baud = 115200;

	while (1)
	{
		static struct option long_options[] =
		{
		{ "_addr_list", required_argument, 0, 'f' },
		{ "_xbee_port", required_argument, 0, 'x' },
		{ "_xbee_baud", required_argument, 0, 'b' },
		{ "help", no_argument, 0, '?' },
		{ 0, 0, 0, 0 } };

		int option_index = 0;
		optarg = NULL;
		c = getopt_long(argc, argv, "f:x:b:", long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		uint16_t tmp;

		switch (c)
		{
		case 'f':
			strncpy(_addr_list_filename, optarg, 128);
			break;
		case 'x':
			strncpy(_xbee_port, optarg, 128);
			break;
		case 'b':
			_xbee_baud = atoi(optarg);
			break;
		case '?':
			puts("Usage:");
			printf("%s -f [address_list] -x [xbee device]\n", argv[0]);
			exit(EXIT_FAILURE);
			break;
		default:
			exit(EXIT_FAILURE);
			break;
		}
	}
}

// allocate the PTYs
void init_pty()
{
	puts("Initializing PTYs...");
	int i;
	for (i = 0; i < _num_addr; ++i)
	{
		// create a pseudoterminal corresponding to each address
		int master, slave;
		pid_t pid = openpty(&master, &slave, NULL, NULL, NULL);
		if (pid == 0)
		{
			// get slave name
			strcpy(_pty_name_list[i], ttyname(slave));
			struct termios opts;
			bzero(&opts, sizeof(opts));
			opts.c_cflag = CRTSCTS | CS8 | CLOCAL | CREAD;
			opts.c_iflag = IGNPAR | IGNBRK;
			opts.c_oflag = 0;
			opts.c_lflag = 0;
			if (tcsetattr(master, TCSANOW, &opts) < 0)
			{
				perror("ERROR setting pty attributes");
				exit(-1);
			}
			_pty_fd_list[i] = master;
			print_address(_addr_list[i], _pty_name_list[i]);
		}
		else
		{
			perror("openpty() failed, exiting.");
			exit(pid);
		}

	}
}

// loop that handles incoming data to all PTYs
void pty_loop()
{
	fd_set set;
	int i;
	size_t n = _num_addr;

	// get maximum fd
	int maxfd = -1;
	for (i = 0; i < n; ++i)
		if (_pty_fd_list[i] > maxfd)
			maxfd = _pty_fd_list[i];

	// incoming buffer
	size_t buf_size = 16;
	uint8_t buf[buf_size];
	mavlink_pkt pkt[n];
	for (i = 0; i < n; ++i)
		mavlink_pkt_init(&pkt[i]);

	while (1)
	{
		// create fd set
		FD_ZERO(&set);
		int i;
		for (i = 0; i < n; ++i)
			FD_SET(_pty_fd_list[i], &set);

		// select on that set
		int r = select(maxfd + 1, &set, NULL, NULL, NULL);
		if (r <= 0)
			break;

		// read data
		for (i = 0; i < n; ++i)
		{
			if (FD_ISSET(_pty_fd_list[i], &set))
			{
				// read some bytes
				int bytes_read = read(_pty_fd_list[i], buf, buf_size);
				if (bytes_read > 0)
				{
					// iterate through read bytes
					int b;
					for (b = 0; b < bytes_read; ++b)
					{
						int pktlen = mavlink_parse(buf[b], &pkt[i]);
						if (pktlen > 0)
						{
							// send through xbee
							xbee_write64(_xbee_fd, pkt[i].data, pkt[i].len,
									_addr_list[i]);

							// reset mavlink packet
							mavlink_pkt_init(&pkt[i]);
						}
					}
				}
			}
		}
	}
	pthread_exit(0);
}

// initialize the xbee serial connection
void init_xbee()
{
	puts("Initializing Xbee serial connection...");
	_xbee_fd = xbee_open(_xbee_port, _xbee_baud);
	if (_xbee_fd < 0)
	{
		fprintf(stderr, "xbee_open could not open %s\n", _xbee_port);
		exit(EXIT_FAILURE);
	}
}

// loop that handles incoming data to xbee
void xbee_loop()
{
	fd_set set;
	FD_ZERO(&set);
	FD_SET(_xbee_fd, &set);

	uint8_t buf[16];
	xbee_pkt pkt;
	xbee_pkt_init(&pkt);

	while (1)
	{
		// wait for bytes
		int r = select(_xbee_fd + 1, &set, NULL, NULL, NULL);
		if (r <= 0)
			break;

		// read bytes
		int bytes_read = read(_xbee_fd, buf, 16);

		int i;
		for (i = 0; i < bytes_read; ++i)
		{
			int pktlen = xbee_parse(buf[i], &pkt);
			if (pktlen > 0)
			{
				pkt.len = xbee_unescape(pkt.data, pkt.len);
				if (pkt.data[3] == XBEE_FRAME_RECV)
				{
					// unpack the sender address
					uint64_t sender_addr = 0;
					sender_addr |= ((((uint64_t) pkt.data[4]) << 56)
							& 0xFF00000000000000);
					sender_addr |= ((((uint64_t) pkt.data[5]) << 48)
							& 0x00FF000000000000);
					sender_addr |= ((((uint64_t) pkt.data[6]) << 40)
							& 0x0000FF0000000000);
					sender_addr |= ((((uint64_t) pkt.data[7]) << 32)
							& 0x000000FF00000000);
					sender_addr |= ((((uint64_t) pkt.data[8]) << 24)
							& 0x00000000FF000000);
					sender_addr |= ((((uint64_t) pkt.data[9]) << 16)
							& 0x0000000000FF0000);
					sender_addr |= ((((uint64_t) pkt.data[10]) << 8)
							& 0x000000000000FF00);
					sender_addr |= (((uint64_t) pkt.data[11])
							& 0x00000000000000FF);

					// unpack the data length
					uint16_t len = 0;
					len |= ((((uint16_t) pkt.data[1]) << 8) & 0xFF00);
					len |= (((uint16_t) pkt.data[2]) & 0x00FF);
					len -= 12; // 12 bytes header

					// if sender address matches one of our addresses,
					// pass the data along.
					int rcpt_idx;
					for (rcpt_idx = 0; rcpt_idx < _num_addr; ++rcpt_idx)
					{
						if (_addr_list[rcpt_idx] == sender_addr)
						{
							write(_pty_fd_list[rcpt_idx], pkt.data + 15, len);
							break;
						}
					}
				}

				// reset xbee packet for next time
				xbee_pkt_init(&pkt);
			}
		}
	}

	pthread_exit(0);
}

// create two threads and join on them.
void create_threads()
{
	pthread_create(&_pty_loop_thread, NULL, (void*) &pty_loop, NULL);
	pthread_create(&_xbee_loop_thread, NULL, (void*) &xbee_loop, NULL);
	puts("Ready for data.");
	pthread_join(_pty_loop_thread, NULL);
	pthread_join(_xbee_loop_thread, NULL);
}

int main(int argc, char**argv)
{
	signal(SIGINT, sigint_handler);
	init_cmdline(argc, argv);
	init_addr_list();
	init_pty();
	init_xbee();
	create_threads();

	// close files
	int i;
	for (i = 0; i < _num_addr; ++i)
		close(_pty_fd_list[i]);
	close(_xbee_fd);

	return 0;
}
