/**
 * Return the addresses of USB-connected xbee radios.
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "xbee_serial.h"
#include <glob.h>

int get_response(uint8_t * response, char * port, int fd)
{
	// keep reading packets until we get the data back
	// or a timer has elapsed.
	fd_set set;
	FD_ZERO(&set);
	FD_SET(fd, &set);

	struct timeval time_limit;
	time_limit.tv_sec = 0;
	time_limit.tv_usec = 500000;

	uint8_t buf[32];
	xbee_pkt pkt;
	xbee_pkt_init(&pkt);
	while (1)
	{
		// wait for data to come in
		int r = select(FD_SETSIZE, &set, NULL, NULL, &time_limit);
		time_limit.tv_sec = 0;
		time_limit.tv_usec = 500000;
		if (r < 0)
		{
			// there was an error
			fprintf(stderr, "Error: Couldn't select() on %s\n", port);
			return -1;
		}
		else if (r == 0)
		{
			fprintf(stderr, "Timeout on %s\n", port);
			return -1;
		}

		// read up to 32 bytes
		int bytes_read = read(fd, buf, 32);
		if (bytes_read > 0)
		{
			// go through each byte
			int b;
			for (b = 0; b < bytes_read; ++b)
			{
				// got a byte
				int len = xbee_parse(buf[b], &pkt);
				if (len > 0)
				{
					// packet found!
					if (pkt.data[3] == 0x88)
					{
						// it was an AT response packet!
						int i, j;
						for (i = 8, j = 0; i < pkt.len - 1; ++i, j += 2)
							sprintf(response + j, "%02X", pkt.data[i]);
						return 0;
					}
					else
					{
						// reset packet, it wasn't an AT response
						xbee_pkt_init(&pkt);
					}
				}
			}
		}
	}
	return 0;
}

void query_version(char * port, int fd)
{
	char addr[17];
	int r = xbee_at_command(fd, "SH", 2);
	if (r < 0)
		return;
	r = get_response(addr, port, fd);
	if (r < 0)
		return;

	r = xbee_at_command(fd, "SL", 2);
	if (r < 0)
		return;
	r = get_response(addr + 8, port, fd);
	if (r < 0)
		return;
	addr[16] = '\0';

	// output the result
	printf("%s: %s\n", port, addr);
}

int main(int argc, char * argv[])
{
	// get a list of serial ports
	const char * sysdir = "/dev/ttyUSB*";
	glob_t glob_data;
	int r = glob(sysdir, 0, NULL, &glob_data);
	if (r != 0)
	{
		fprintf(stderr, "Error: Could not get the list of serial ports.");
		exit(-1);
	}
	if (glob_data.gl_pathc <= 0)
	{
		fprintf(stderr, "Error: No serial ports found.");
		exit(-1);
	}

	// check each serial port for xbee device
	int i;
	for (i = 0; i < glob_data.gl_pathc; ++i)
	{
		int fd = xbee_open(glob_data.gl_pathv[i], 115200);
		if (fd < 0)
			continue;
		query_version(glob_data.gl_pathv[i], fd);

		close(fd);
	}

	// clean up
	globfree(&glob_data);

	return 0;
}

