/**
 * simple mavlink parser
 * does not interpret data, just checks for complete packets
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <assert.h>

typedef enum {
	MAVLINK_PARSE_IDLE,
	MAVLINK_PARSE_LEN,
	MAVLINK_PARSE_PAYLOAD,
	MAVLINK_PARSE_CRC,
	MAVLINK_PARSE_COMPLETE,
} mavlink_parse_state;

typedef struct {
	mavlink_parse_state state;
	uint16_t expected_len;
	int len;
	uint8_t data[1024];
} mavlink_pkt;

void mavlink_pkt_init(mavlink_pkt * pkt);
int mavlink_parse(uint8_t c, mavlink_pkt * pkt);
