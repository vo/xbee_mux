#include "xbee_serial.h"

int main(int argc, char * argv[])
{

	int fd;
	int r;
	char port[] = "/dev/ttyUSB0";
	int baud = 115200;

	fd = xbee_open(port, baud);
	if (fd < 0)
		exit(fd);

	unsigned char buf[255];
	int len = load_asciihex(buf, "DE AD BE EF F0 0D") + 2;

	uint16_t i;
	for (i = 0; i < 100; ++i)
	{
		printf("sending packet %d\n", i);
		buf[len - 2] = (uint8_t) (i >> 8); // MSB
		buf[len - 1] = (uint8_t) (i); // LSB
		xbee_write64(fd, buf, len, XBEE_ADDR_BROADCAST);
	}
	close(fd);

	return 0;
}
